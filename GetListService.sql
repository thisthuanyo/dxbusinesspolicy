﻿CREATE PROC GetListService 
AS
BEGIN
	SELECT dbo.DXBusinessPolicy_Service.Code,
		   dbo.DXBusinessPolicy_Service.Name
	FROM dbo.DXBusinessPolicy_Service
END
