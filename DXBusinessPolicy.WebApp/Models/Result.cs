﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DXBusinessPolicy.WebApp.Models
{
    public class Result
    {
        public int StatusCode { get; set; }
        public string Message { get; set; }
    }
}