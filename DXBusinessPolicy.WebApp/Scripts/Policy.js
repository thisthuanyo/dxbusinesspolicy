﻿$().ready(function () {

    var services = $.ajax({
        type: "GET",
        url: '/Home/GetListService/',
        async: false
    }).responseJSON;

    var dialog = new BootstrapDialog({
        title: 'Pilicy Filter',
        autodestroy: true,
        message: '<select id="multiselect" data-placeholder="Select..." multiple="multiple"></select><div id="grid"></div>',
        onshown: function () {

            // Load services to Select Box
            $("#multiselect").kendoMultiSelect({
                dataSource: services,
                dataTextField: "Name",
            });

            //Event option change - filter Policies by Service
            $('#multiselect').on('change', function () {
                var multiselect = $("#multiselect").data("kendoMultiSelect");
               
                // get data items for the selected options.
                var dataItem = multiselect.dataItems();

                var codes = '';
                
                dataItem.forEach(function (item) {
                    codes += item.Code + ",";
                });

                var vm = kendo.observable({
                    dataSource: new kendo.data.DataSource({
                        transport: {
                            read: function (options) {
                                $.ajax({
                                    type: 'POST',
                                    url: '/Home/Read',
                                    data: { 'codes': codes },
                                    type: "POST",
                                    success: function (result) {
                                        options.success(result);
                                    }
                                });
                            },
                            update: function (options) {
                                $.ajax({
                                    url: '/Home/Update',
                                    dataType: "json",
                                    type: "POST",
                                    data: options.data, 
                                    success: function (result) {
                                        alert(result.Message);
                                        if (result.StatusCode === 1) {
                                            options.success();
                                        } else {
                                            $('#grid').data("kendoGrid").cancelChanges(); //Undo changes
                                        }
                                    }
                                });
                            },
                        },
                        parameterMap: function (options, operation) {
                            if (operation !== "read" && options.models) {
                                return { models: kendo.stringify(options.models) };
                            }
                        },
                       
                    pageSize: 7,
                    autoSync: true,
                    schema: {
                        model: {
                            id: 'PolicyID',
                            fields: {
                                'PolicyCode': { editable: false },
                                'PolicyName': {
                                    editable: true,
                                    validation: {
                                        required: true
                                    }
                                },
                                'Services': {},
                            }
                        }
                    }}),
                });

                var multiSelectArrayToString = function (item) {
                    return item.Services.join(', ');
                };

                $("#grid").kendoGrid({
                    dataSource: vm.dataSource,
                    editable: "incell",
                    pageable: true,
                    sortable: true,
                    filterable: {
                        mode: "menu",
                        extra: false
                    },
                    columns: [{
                        field: "PolicyCode",
                        title: "Mã chính sách",
                    }, {
                        field: "PolicyName",
                        title: "Tên chính sách",
                    },
                    {
                        field: "Services",
                        title: "DS dịch vụ",
                        editor: multiServiceEditor,
                        template: multiSelectArrayToString,
                    }],
                });
            });

            function multiServiceEditor(container, options) {
                $('<select data-bind="value:' + options.field + '"/>')
                    .appendTo(container)
                    .kendoMultiSelect({
                        suggest: true,
                        dataSource: services,
                        dataTextField: "Name",
                        dataValueField: "Code",
                        valuePrimitive: true
                    });
            }

            $(".modal").removeAttr("tabindex"); // Bug fix: cant enter value into textbox in BootstrapDialog 
        },
    });
   
    $('#btn-open').on('click', { dialog: dialog }, function (event) {
        event.data.dialog.open();
    });
    
})
