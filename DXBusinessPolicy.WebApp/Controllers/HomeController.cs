﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using DXBusinessPolicy.Data.Model;
using Microsoft.Ajax.Utilities;
using System.Text;
using DXBusinessPolicy.WebApp.Models;

namespace DXBusinessPolicy.WebApp.Controllers
{
    public class HomeController : Controller
    {
        private string BaseAddress = ConfigurationManager.AppSettings["BaseAddress"].ToString();
        private static HttpClient hc = new HttpClient();

        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetListService()
        {
            string url = BaseAddress + "/api/Service";
            var result = hc.GetAsync(url);
            result.Wait();
            if (result.Result.IsSuccessStatusCode == false)
                return Json(null, JsonRequestBehavior.AllowGet);
            else
            {
                var servicesRespone = result.Result.Content.ReadAsStringAsync().Result;
                return Json(JsonConvert.DeserializeObject<List<ServiceViewModel>>(servicesRespone), JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult Read(string codes)
        {

            string url = BaseAddress + $"/api/Policy?codes={codes}";
            var respone = hc.GetAsync(url);
            respone.Wait();
            if (respone.Result.IsSuccessStatusCode)
            {
                var result = respone.Result.Content.ReadAsStringAsync().Result;
                var objResult = JsonConvert.DeserializeObject<List<PolicyServiceViewModel>>(result);
                var policies = new List<PolicyViewModel>();
                foreach (var item in objResult)
                {
                    var Policy = new PolicyViewModel
                    {
                        PolicyID = item.PolicyID,
                        PolicyCode = item.PolicyCode,
                        PolicyName = item.PolicyName,
                        Services = item.ServiceCode.Split(',').ToList()
                    };
                    policies.Add(Policy);
                }

                return Json(policies, JsonRequestBehavior.AllowGet);
            }
            return Json(null, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult Update(PolicyViewModel PV)
        {
            string url = BaseAddress + "/api/Policy";

            var request = new PolicyUpdateRequest
            {
                PolicyID = PV.PolicyID,
                PolicyName = PV.PolicyName,
                Services = PV.Services != null ? String.Join(",", PV.Services) : ""
            };

            var json = JsonConvert.SerializeObject(request);

            var httpContent = new StringContent(json, Encoding.UTF8, "application/json");

            var result = hc.PutAsync(url, httpContent);
            result.Wait();

            var respone = result.Result.Content.ReadAsStringAsync().Result;

            if (result.Result.IsSuccessStatusCode == false)
            {
                var res = new Result
                {
                    StatusCode = 0,
                    Message = respone
                };
                return Json(res, JsonRequestBehavior.AllowGet);
            } else
            {
                var res = new Result
                {
                    StatusCode = 1,
                    Message = respone
                };
                return Json(res, JsonRequestBehavior.AllowGet);
            }
        }
    }
}