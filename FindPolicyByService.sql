﻿ALTER PROC FindPolicyByService
    @codes varchar(200)
AS 
BEGIN
   SELECT p.ID AS PolicyID,
       p.Code AS PolicyCode,
       p.Name AS PolicyName,
       STRING_AGG(s.Code, ',') AS ServiceCode
	FROM dbo.DXBusinessPolicy_Policy AS p
	JOIN dbo.DXBusinessPolicy_PolicyService AS ps ON p.ID = ps.PolicyID
	JOIN dbo.DXBusinessPolicy_Service AS s ON ps.ServiceID = s.ID
	WHERE p.ID IN 
	(
		SELECT DISTINCT subps.PolicyID
		FROM dbo.DXBusinessPolicy_PolicyService AS subps
		JOIN dbo.DXBusinessPolicy_Service AS subs ON subps.ServiceID = subs.ID
		WHERE subs.Code IN (SELECT VALUE FROM string_split(@codes,','))
	)
	GROUP by p.ID, p.Code, p.Name
END

exec FindPolicyByService "Int,IPTV"
