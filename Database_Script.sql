﻿Create database DXBusinessPolicy
GO
use DXBusinessPolicy
GO
--
-- Create table [dbo].[DXBusinessPolicy_Service]
--
PRINT (N'Create table [dbo].[DXBusinessPolicy_Service]')
GO
CREATE TABLE dbo.DXBusinessPolicy_Service (
  ID int NOT NULL,
  Code varchar(50) NULL,
  Name nvarchar(100) NULL,
  Description nvarchar(1000) NULL,
  Type int NULL,
  Status int NULL,
  CONSTRAINT PK_DXBusinessPolicy_Service_ID PRIMARY KEY CLUSTERED (ID)
)
ON [PRIMARY]
GO

--
-- Create table [dbo].[DXBusinessPolicy_Policy]
--
PRINT (N'Create table [dbo].[DXBusinessPolicy_Policy]')
GO
CREATE TABLE dbo.DXBusinessPolicy_Policy (
  ID int IDENTITY,
  Code varchar(50) NULL,
  Name nvarchar(512) NULL,
  PolicyGroupID int NULL,
  PolicyTypeID int NULL,
  Type int NULL,
  SuggesttionID int NULL,
  CreateDate datetime NULL,
  CreateBy varchar(50) NULL,
  ModifyDate datetime NULL,
  ModifyBy varchar(50) NULL,
  Description nvarchar(2000) NULL,
  Status int NULL,
  FromDate datetime NULL,
  ToDate datetime NULL,
  UpdateBy varchar(50) NULL,
  UpdateDate datetime NULL,
  StatusApprove int NULL,
  RequestBy varchar(50) NULL,
  RequestDate datetime NULL,
  PolicyBase nvarchar(1005) NULL,
  CONSTRAINT PK_DXBusinessPolicy_Policy PRIMARY KEY CLUSTERED (ID)
)
ON [PRIMARY]
GO

--
-- Create table [dbo].[DXBusinessPolicy_PolicyService]
--
PRINT (N'Create table [dbo].[DXBusinessPolicy_PolicyService]')
GO
CREATE TABLE dbo.DXBusinessPolicy_PolicyService (
  ID int IDENTITY,
  PolicyID int NULL,
  ServiceID int NULL,
  CONSTRAINT PK_DXBusinessPolicy_PolicyService PRIMARY KEY CLUSTERED (ID)
)
ON [PRIMARY]
GO
-- 
-- Dumping data for table DXBusinessPolicy_Policy
--
PRINT (N'Dumping data for table DXBusinessPolicy_Policy')
SET IDENTITY_INSERT dbo.DXBusinessPolicy_Policy ON
GO
INSERT dbo.DXBusinessPolicy_Policy(ID, Code, Name, PolicyGroupID, PolicyTypeID, Type, SuggesttionID, CreateDate, CreateBy, ModifyDate, ModifyBy, Description, Status, FromDate, ToDate, UpdateBy, UpdateDate, StatusApprove, RequestBy, RequestDate, PolicyBase) VALUES (1, 'COMBO.2103001', N'[Giá nền] T9/2020 #1', 1, 1, 1, 0, '2021-03-02 16:23:21.293', 'KetDV', '2021-03-02 16:25:24.770', 'string', N'string', 7, '2021-03-02 09:22:46.000', '2021-09-02 09:22:46.000', 'oanh254', '2021-03-18 08:57:15.523', NULL, 'KetDV', '2021-03-19 15:23:35.537', NULL)
INSERT dbo.DXBusinessPolicy_Policy(ID, Code, Name, PolicyGroupID, PolicyTypeID, Type, SuggesttionID, CreateDate, CreateBy, ModifyDate, ModifyBy, Description, Status, FromDate, ToDate, UpdateBy, UpdateDate, StatusApprove, RequestBy, RequestDate, PolicyBase) VALUES (2, 'IPTV-0121.002', N'[Giá nền] T8/2020 #1', 1, 1, 2, 0, '2021-03-02 16:24:58.643', 'KhanhHC2', NULL, NULL, N'string', 1, '2021-03-02 09:22:46.000', '2021-03-02 09:22:46.000', 'KetDv', NULL, NULL, 'KhanhHC2', NULL, NULL)
INSERT dbo.DXBusinessPolicy_Policy(ID, Code, Name, PolicyGroupID, PolicyTypeID, Type, SuggesttionID, CreateDate, CreateBy, ModifyDate, ModifyBy, Description, Status, FromDate, ToDate, UpdateBy, UpdateDate, StatusApprove, RequestBy, RequestDate, PolicyBase) VALUES (3, 'INT.2103001', N'Chính sách 2', 1, 1, 2, 0, '2021-03-12 15:40:37.070', 'KhanhHC2', '2021-03-12 15:41:11.370', NULL, N'Ghi chú áp dụng', 6, '2021-03-01 00:00:00.000', '2021-03-31 00:00:00.000', '', '2021-03-18 09:38:29.310', NULL, 'KhanhHC2', '2021-03-19 15:23:35.537', NULL)
INSERT dbo.DXBusinessPolicy_Policy(ID, Code, Name, PolicyGroupID, PolicyTypeID, Type, SuggesttionID, CreateDate, CreateBy, ModifyDate, ModifyBy, Description, Status, FromDate, ToDate, UpdateBy, UpdateDate, StatusApprove, RequestBy, RequestDate, PolicyBase) VALUES (4, 'INT.2103002', N'Giá nền INT only T04.2021', 1, 1, 2, 0, '2021-03-15 11:00:31.150', 'oanh254', NULL, NULL, NULL, 2, '2021-04-01 00:00:00.000', '2030-01-01 00:00:00.000', 'oanh254', '2021-03-22 15:41:46.220', NULL, 'oanh254', '2021-03-19 15:23:35.537', NULL)
INSERT dbo.DXBusinessPolicy_Policy(ID, Code, Name, PolicyGroupID, PolicyTypeID, Type, SuggesttionID, CreateDate, CreateBy, ModifyDate, ModifyBy, Description, Status, FromDate, ToDate, UpdateBy, UpdateDate, StatusApprove, RequestBy, RequestDate, PolicyBase) VALUES (5, 'INT.2103003', N'Giá nền INT only T04.2021', 1, 1, 2, 0, '2021-03-15 11:00:49.290', 'oanh254', NULL, NULL, NULL, 2, '2021-04-01 00:00:00.000', '2030-01-01 00:00:00.000', 'oanh254', '2021-03-23 09:07:28.123', NULL, 'oanh254', '2021-03-19 15:23:35.537', NULL)
INSERT dbo.DXBusinessPolicy_Policy(ID, Code, Name, PolicyGroupID, PolicyTypeID, Type, SuggesttionID, CreateDate, CreateBy, ModifyDate, ModifyBy, Description, Status, FromDate, ToDate, UpdateBy, UpdateDate, StatusApprove, RequestBy, RequestDate, PolicyBase) VALUES (6, 'INT.2103004', N'Giá nền INT only T04.2021', 1, 1, 2, 0, '2021-03-15 11:01:05.993', 'oanh254', '2021-03-18 09:08:09.553', 'Trannnt2', NULL, 5, '2021-04-01 00:00:00.000', '2030-01-01 00:00:00.000', '', '2021-03-19 11:01:57.250', NULL, 'oanh254', '2021-03-19 15:23:35.537', NULL)
INSERT dbo.DXBusinessPolicy_Policy(ID, Code, Name, PolicyGroupID, PolicyTypeID, Type, SuggesttionID, CreateDate, CreateBy, ModifyDate, ModifyBy, Description, Status, FromDate, ToDate, UpdateBy, UpdateDate, StatusApprove, RequestBy, RequestDate, PolicyBase) VALUES (7, 'IPTV.2103001', N'Test 01', 1, 1, 2, 0, '2021-03-15 11:09:24.730', 'oanh254', '2021-03-15 15:07:10.040', 'oanh254', N'test', 5, '2021-03-01 00:00:00.000', NULL, 'oanh254', '2021-03-22 11:02:24.573', NULL, 'oanh254', '2021-03-19 15:23:35.537', NULL)
INSERT dbo.DXBusinessPolicy_Policy(ID, Code, Name, PolicyGroupID, PolicyTypeID, Type, SuggesttionID, CreateDate, CreateBy, ModifyDate, ModifyBy, Description, Status, FromDate, ToDate, UpdateBy, UpdateDate, StatusApprove, RequestBy, RequestDate, PolicyBase) VALUES (8, 'IPTV.2103002', N'Test 02', 1, 1, 2, 0, '2021-03-15 11:19:26.290', 'oanh254', NULL, NULL, N'T', 6, '2021-03-01 00:00:00.000', '2021-03-31 00:00:00.000', '', '2021-03-19 11:08:49.180', NULL, 'oanh254', '2021-03-19 15:23:35.537', NULL)
INSERT dbo.DXBusinessPolicy_Policy(ID, Code, Name, PolicyGroupID, PolicyTypeID, Type, SuggesttionID, CreateDate, CreateBy, ModifyDate, ModifyBy, Description, Status, FromDate, ToDate, UpdateBy, UpdateDate, StatusApprove, RequestBy, RequestDate, PolicyBase) VALUES (9, 'IPTV.2103003', N'Test 03', 1, 1, 2, 0, '2021-03-15 11:22:36.913', 'oanh254', NULL, NULL, N'T', 6, '2021-03-01 00:00:00.000', '2021-03-31 00:00:00.000', 'oanh254', '2021-03-22 11:02:36.623', NULL, 'oanh254', '2021-03-19 15:23:35.537', NULL)
INSERT dbo.DXBusinessPolicy_Policy(ID, Code, Name, PolicyGroupID, PolicyTypeID, Type, SuggesttionID, CreateDate, CreateBy, ModifyDate, ModifyBy, Description, Status, FromDate, ToDate, UpdateBy, UpdateDate, StatusApprove, RequestBy, RequestDate, PolicyBase) VALUES (10, 'INT.2103005', N'Giá nền INT only T04.2021', 1, 1, 2, 0, '2021-03-15 15:31:27.080', 'Trannnt2', '2021-03-15 16:20:17.313', 'Trannnt2', NULL, 4, '2021-04-01 00:00:00.000', NULL, 'KhanhHC2', '2021-03-22 16:34:48.013', NULL, 'Trannnt2', '2021-03-22 15:34:48.013', N'mail 123')
INSERT dbo.DXBusinessPolicy_Policy(ID, Code, Name, PolicyGroupID, PolicyTypeID, Type, SuggesttionID, CreateDate, CreateBy, ModifyDate, ModifyBy, Description, Status, FromDate, ToDate, UpdateBy, UpdateDate, StatusApprove, RequestBy, RequestDate, PolicyBase) VALUES (11, 'INT.2103006', N'Giá nền INT only T04.2021 Vùng 1', 1, 1, 2, 0, '2021-03-16 09:32:26.710', 'Trannnt2', '2021-03-16 09:46:47.560', 'Trannnt2', NULL, 5, '2021-04-01 00:00:00.000', NULL, NULL, NULL, NULL, 'Trannnt2', '2021-03-22 09:32:26.710', NULL)
INSERT dbo.DXBusinessPolicy_Policy(ID, Code, Name, PolicyGroupID, PolicyTypeID, Type, SuggesttionID, CreateDate, CreateBy, ModifyDate, ModifyBy, Description, Status, FromDate, ToDate, UpdateBy, UpdateDate, StatusApprove, RequestBy, RequestDate, PolicyBase) VALUES (12, 'COMBO.2103002', N'Giá nền combo T04.2021', 1, 1, 1, 0, '2021-03-16 09:49:41.833', 'Trannnt2', NULL, NULL, NULL, 3, '2021-04-01 00:00:00.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT dbo.DXBusinessPolicy_Policy(ID, Code, Name, PolicyGroupID, PolicyTypeID, Type, SuggesttionID, CreateDate, CreateBy, ModifyDate, ModifyBy, Description, Status, FromDate, ToDate, UpdateBy, UpdateDate, StatusApprove, RequestBy, RequestDate, PolicyBase) VALUES (13, 'COMBO.2103003', N'Combo T5.2021', 1, 1, 1, 0, '2021-03-18 09:09:29.887', 'Trannnt2', NULL, NULL, NULL, 3, '2021-05-01 00:00:00.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT dbo.DXBusinessPolicy_Policy(ID, Code, Name, PolicyGroupID, PolicyTypeID, Type, SuggesttionID, CreateDate, CreateBy, ModifyDate, ModifyBy, Description, Status, FromDate, ToDate, UpdateBy, UpdateDate, StatusApprove, RequestBy, RequestDate, PolicyBase) VALUES (14, 'COMBO.2103004', N'FTQ Test chính sách', 0, 0, 1, 0, '2021-03-22 16:31:43.923', 'trangntt93', NULL, NULL, NULL, 3, '2021-03-01 00:00:00.000', '2021-03-31 00:00:00.000', NULL, NULL, NULL, NULL, NULL, NULL)
GO
SET IDENTITY_INSERT dbo.DXBusinessPolicy_Policy OFF
GO
-- 
-- Dumping data for table DXBusinessPolicy_PolicyService
--
PRINT (N'Dumping data for table DXBusinessPolicy_PolicyService')
SET IDENTITY_INSERT dbo.DXBusinessPolicy_PolicyService ON
GO
INSERT dbo.DXBusinessPolicy_PolicyService(ID, PolicyID, ServiceID) VALUES (2, 1, 1)
INSERT dbo.DXBusinessPolicy_PolicyService(ID, PolicyID, ServiceID) VALUES (3, 1, 2)
INSERT dbo.DXBusinessPolicy_PolicyService(ID, PolicyID, ServiceID) VALUES (4, 3, 1)
INSERT dbo.DXBusinessPolicy_PolicyService(ID, PolicyID, ServiceID) VALUES (5, 4, 1)
INSERT dbo.DXBusinessPolicy_PolicyService(ID, PolicyID, ServiceID) VALUES (6, 5, 1)
INSERT dbo.DXBusinessPolicy_PolicyService(ID, PolicyID, ServiceID) VALUES (7, 6, 1)
INSERT dbo.DXBusinessPolicy_PolicyService(ID, PolicyID, ServiceID) VALUES (8, 7, 2)
INSERT dbo.DXBusinessPolicy_PolicyService(ID, PolicyID, ServiceID) VALUES (9, 8, 2)
INSERT dbo.DXBusinessPolicy_PolicyService(ID, PolicyID, ServiceID) VALUES (10, 9, 2)
INSERT dbo.DXBusinessPolicy_PolicyService(ID, PolicyID, ServiceID) VALUES (11, 10, 1)
INSERT dbo.DXBusinessPolicy_PolicyService(ID, PolicyID, ServiceID) VALUES (12, 11, 1)
INSERT dbo.DXBusinessPolicy_PolicyService(ID, PolicyID, ServiceID) VALUES (13, 12, 1)
INSERT dbo.DXBusinessPolicy_PolicyService(ID, PolicyID, ServiceID) VALUES (14, 12, 2)
INSERT dbo.DXBusinessPolicy_PolicyService(ID, PolicyID, ServiceID) VALUES (15, 12, 3)
INSERT dbo.DXBusinessPolicy_PolicyService(ID, PolicyID, ServiceID) VALUES (16, 12, 4)
INSERT dbo.DXBusinessPolicy_PolicyService(ID, PolicyID, ServiceID) VALUES (17, 13, 1)
INSERT dbo.DXBusinessPolicy_PolicyService(ID, PolicyID, ServiceID) VALUES (18, 13, 2)
INSERT dbo.DXBusinessPolicy_PolicyService(ID, PolicyID, ServiceID) VALUES (19, 13, 3)
INSERT dbo.DXBusinessPolicy_PolicyService(ID, PolicyID, ServiceID) VALUES (20, 13, 4)
INSERT dbo.DXBusinessPolicy_PolicyService(ID, PolicyID, ServiceID) VALUES (21, 14, 1)
INSERT dbo.DXBusinessPolicy_PolicyService(ID, PolicyID, ServiceID) VALUES (22, 14, 2)
GO
SET IDENTITY_INSERT dbo.DXBusinessPolicy_PolicyService OFF
GO
-- 
-- Dumping data for table DXBusinessPolicy_Service
--
PRINT (N'Dumping data for table DXBusinessPolicy_Service')
INSERT dbo.DXBusinessPolicy_Service VALUES (1, 'INT', N'Internet', NULL, NULL, 1)
INSERT dbo.DXBusinessPolicy_Service VALUES (2, 'IPTV', N'IPTV', NULL, NULL, 1)
INSERT dbo.DXBusinessPolicy_Service VALUES (3, 'CMR', N'Camera', NULL, NULL, 1)
INSERT dbo.DXBusinessPolicy_Service VALUES (4, 'OTT', N'OTT', NULL, NULL, 1)
INSERT dbo.DXBusinessPolicy_Service VALUES (5, 'FSAFE', N'Fsafe', NULL, NULL, 1)
INSERT dbo.DXBusinessPolicy_Service VALUES (6, 'IP', N'IP tĩnh', NULL, NULL, 1)
GO
SET NOEXEC OFF
GO