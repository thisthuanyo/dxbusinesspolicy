﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DXBusinessPolicy.Data.Model
{
    public class Policy
    {
        public int ID { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public int PolicyGroupID { get; set; }
        public int PolicyTypeID { get; set; }
        public int Type { get; set; }
        public int SuggettionID { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreateBy { get; set; }
        public DateTime ModifyDate { get; set; }
        public string ModifyBy { get; set; }
        public string Description { get; set; }
        public int Status { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate{ get; set; }
        public string UpdateBy { get; set; }
        public int StatusApprove { get; set; }
        public string RequestBy { get; set; }
        public DateTime RequestDate { get; set; }
        public string PolicyBase { get; set; }
    }
}
