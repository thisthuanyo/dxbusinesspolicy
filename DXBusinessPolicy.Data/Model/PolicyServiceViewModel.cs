﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DXBusinessPolicy.Data.Model
{
    public class PolicyServiceViewModel
    {
        public int PolicyID { get; set; }
        public string PolicyCode { get; set; }
        public string PolicyName { get; set; }
        public string ServiceCode { get; set; }
    }
}
