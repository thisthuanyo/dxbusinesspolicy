﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DXBusinessPolicy.Data.Model
{
    public class ServiceViewModel
    {
        public string Code { get; set; }
        public string Name { get; set; }
    }
}
