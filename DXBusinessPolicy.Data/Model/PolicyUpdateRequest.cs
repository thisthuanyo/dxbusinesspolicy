﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DXBusinessPolicy.Data.Model
{
    public class PolicyUpdateRequest
    {
        public int PolicyID { get; set; }
        public string PolicyName { get; set; }
        public string Services { get; set; }
    }
}
