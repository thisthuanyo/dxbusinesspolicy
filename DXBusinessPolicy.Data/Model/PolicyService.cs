﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DXBusinessPolicy.Data.Model
{
    public class PolicyService
    {
        public int ID { get; set; }
        public int PolicyID { get; set; }
        public int ServiceID { get; set; }
    }
}
