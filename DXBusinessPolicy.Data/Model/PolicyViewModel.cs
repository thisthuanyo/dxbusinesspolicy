﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DXBusinessPolicy.Data.Model
{
    public class PolicyViewModel
    {
        public int PolicyID { get; set; }
        public string PolicyCode { get; set; }
        public string PolicyName { get; set; }
        public List<string> Services { get; set; }
    }
}
