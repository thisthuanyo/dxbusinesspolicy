﻿using Dapper;
using DXBusinessPolicy.Data.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DXBusinessPolicy.API.Controllers
{
    public class ServiceController : ApiController
    {
        [HttpGet]
        public IHttpActionResult GetListService()
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["DXBusinessPolicyDB"].ToString()))
            {
                var parm = new DynamicParameters();
                try
                {
                    sqlConnection.Open();
                    var result = (List<ServiceViewModel>)
                        sqlConnection.Query<ServiceViewModel>("GetListService", parm, commandType: System.Data.CommandType.StoredProcedure);
                    sqlConnection.Close();
                    return Ok(result);
                }
                catch (Exception ex)
                {
                    return BadRequest(ex.Message);
                }
            }
        }
    }
}
