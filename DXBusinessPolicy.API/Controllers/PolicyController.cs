﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Web.Http;
using DXBusinessPolicy.Data.Model;
using System.Data;

namespace DXBusinessPolicy.API.Controllers
{
    public class PolicyController : ApiController
    {
        [HttpGet]
        public IHttpActionResult findPolicyByService(string codes)
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["DXBusinessPolicyDB"].ToString()))
            {
                var parm = new DynamicParameters();
                parm.Add("@codes", codes);
                try
                {
                    sqlConnection.Open();
                    var result = (List<PolicyServiceViewModel>)
                        sqlConnection.Query<PolicyServiceViewModel>("FindPolicyByService", parm, commandType: CommandType.StoredProcedure);
                    sqlConnection.Close();
                    return Json(result);
                }
                catch (Exception ex)
                {
                    return BadRequest(ex.Message);
                }
            }    
        }
        [HttpPut]
        public IHttpActionResult Update([FromBody]PolicyUpdateRequest request)
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["DXBusinessPolicyDB"].ToString()))
            {
                var parm = new DynamicParameters();
                parm.Add("@id", request.PolicyID);
                parm.Add("@name", request.PolicyName);
                parm.Add("@services", request.Services);
                parm.Add("@result", dbType: DbType.Int32, direction: ParameterDirection.Output);
                try
                {
                    sqlConnection.Open();
                    sqlConnection.Execute("UpdatePolicy", parm, commandType: CommandType.StoredProcedure);
                    var b = parm.Get<int>("@result");
                    sqlConnection.Close();
                    if(b == 1)
                        return Ok("Update Successfully!");
                    else
                        return BadRequest("Update Failed!");
                }
                catch (Exception ex)
                {
                    return BadRequest(ex.Message);
                }
            }
        }
    }
}
