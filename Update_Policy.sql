﻿CREATE PROCEDURE UpdatePolicy
	@id int,
	@name nvarchar(512),
	@services varchar(1000),
	@result int output
AS 
BEGIN 
	
	BEGIN TRANSACTION
		UPDATE dbo.DXBusinessPolicy_Policy
		SET Name =  @name, ModifyDate = GETDATE()
		WHERE ID = @id 

		DELETE FROM dbo.DXBusinessPolicy_PolicyService
		WHERE dbo.DXBusinessPolicy_PolicyService.PolicyID = @id 

		INSERT INTO dbo.DXBusinessPolicy_PolicyService (PolicyID, ServiceID) 
		SELECT @id, dbo.DXBusinessPolicy_Service.ID 
		FROM dbo.DXBusinessPolicy_Service
		WHERE dbo.DXBusinessPolicy_Service.Code IN (
			SELECT VALUE FROM string_split(@services,','))

	 IF @@ERROR != 0 
		 BEGIN
			ROLLBACK TRANSACTION
			SET @result = 0
		 END
	ELSE 
	 BEGIN
			SET @result = 1
			COMMIT TRANSACTION
	 END
END 

exec UpdatePolicy 14, 'FTQ Test chính sách', 'FSAFE,IP'







